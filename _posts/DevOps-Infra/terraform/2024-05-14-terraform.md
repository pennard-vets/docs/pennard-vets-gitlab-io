---
title: terraform
date: 2024-05-14 14:59:00 +0100
categories: [terraform,setup]
tags: [terraform,docs,setup]     # TAG names should always be lowercase
pin: false
---

## Links

Useful links👇

- [Terraform Providers](https://registry.terraform.io/providers/hashicorp/aws/latest/docs)
- [GitLab project](https://gitlab.com/paulkurpis/terraform-learn/-/tree/main)

### install terraform in ubuntu

Update your package list and Install the required packages:

```bash
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common
```

Add the HashiCorp GPG key:

```bash
wget -O- https://apt.releases.hashicorp.com/gpg | \
gpg --dearmor | \
sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg > /dev/null
```

Add the HashiCorp repository:

```bash
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
sudo tee /etc/apt/sources.list.d/hashicorp.list
```

Update your package list again to include the new repository and Install Terraform:

```bash
sudo apt-get update && sudo apt-get install terraform
```

Verify the installation (optional):

```bash
terraform -v
```

### Terraform Init

set terminal where is your project: `cd terraform/main.tf`

```bash
terraform init
```

```bash
terraform plan
```

```bash
terraform apply -auto-approve
```

Other commands

```bash
terraform destroy
```

```bash
terraform destroy -target <target_name>
```

```bash
terraform apply -var-file
```

```bash
terraform state list
```

```bash
terraform state show
```
